//
//  main.m
//  DropShadowDemo
//
//  Created by Scott Ferwerda on 11/22/16.
//  Copyright © 2016 High Country Computer Consulting, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
