//
//  HCCBorderedView.h
//  DropShadowDemo
//
//  Created by Scott Ferwerda on 11/22/16.
//  Copyright © 2016 High Country Computer Consulting, Inc. All rights reserved.
//
//  An Interface Builder - editable UIView subclass, with access to border and shadow settings.
//  Thanks to sample code from http://nshipster.com/ibinspectable-ibdesignable/
//  and http://stackoverflow.com/questions/9761189/whats-the-best-way-to-add-a-drop-shadow-to-my-uiview
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface HCCBorderedView : UIView

// NOTE: a nonzero border radius will disable the drop shadow.
@property (nonatomic, assign) IBInspectable CGFloat borderRadius;
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, strong) IBInspectable UIColor *borderColor;
@property (nonatomic, assign) IBInspectable CGFloat shadowOpacity;
@property (nonatomic, assign) IBInspectable CGFloat shadowRadius;
@property (nonatomic, assign) IBInspectable CGSize shadowOffset;
@property (nonatomic, strong) IBInspectable UIColor *shadowColor;


@end
