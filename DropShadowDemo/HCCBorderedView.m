//
//  HCCBorderedView.m
//  DropShadowDemo
//
//  Created by Scott Ferwerda on 11/22/16.
//  Copyright © 2016 High Country Computer Consulting, Inc. All rights reserved.
//

#import "HCCBorderedView.h"

@implementation HCCBorderedView

- (CGFloat)borderRadius
{
    return self.layer.cornerRadius;
}

- (void)setBorderRadius:(CGFloat)cornerRadius
{
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = (cornerRadius > 0);
}

- (CGFloat)borderWidth
{
    return self.layer.borderWidth;
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    self.layer.borderWidth = borderWidth;
}

- (UIColor *)borderColor
{
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void)setBorderColor:(UIColor *)borderColor
{
    self.layer.borderColor = [borderColor CGColor];
}

- (CGFloat)shadowOpacity
{
    return self.layer.shadowOpacity;
}

- (void)setShadowOpacity:(CGFloat)shadowOpacity
{
    self.layer.shadowOpacity = shadowOpacity;
}

- (CGFloat)shadowRadius
{
    return self.layer.shadowRadius;
}

- (void)setShadowRadius:(CGFloat)shadowRadius
{
    self.layer.shadowRadius = shadowRadius;
    if (shadowRadius > 0) {
        self.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.bounds] CGPath];
    }
    else {
        self.layer.shadowPath = NULL;
    }
}

- (CGSize)shadowOffset
{
    return self.layer.shadowOffset;
}

- (void)setShadowOffset:(CGSize)shadowOffset
{
    self.layer.shadowOffset = shadowOffset;
}

- (UIColor *)shadowColor
{
    return [UIColor colorWithCGColor:self.layer.shadowColor];
}

- (void)setShadowColor:(UIColor *)shadowColor
{
    self.layer.shadowColor = [shadowColor CGColor];
}

@end
