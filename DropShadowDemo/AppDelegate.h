//
//  AppDelegate.h
//  DropShadowDemo
//
//  Created by Scott Ferwerda on 11/22/16.
//  Copyright © 2016 High Country Computer Consulting, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

